import { HttpClient } from '@angular/common/http';
import { APIKEY } from '../conf/conf';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Movie } from 'src/model/logical/movie';
import { TvShow } from 'src/model/logical/tvShow';

@Injectable({
    providedIn: 'root',
})
export class SearchService {

  searchTerms = new BehaviorSubject<string | undefined>(undefined);

  typeSliderIsTV = new BehaviorSubject<boolean | undefined>(undefined);

  typeSliderIsMovie = new BehaviorSubject<boolean | undefined>(undefined);

  selectedYears = new BehaviorSubject<number[] | undefined>(undefined);

  constructor(private httpClient: HttpClient) {}

  updateSearchTerms(terms: string) {
    this.searchTerms.next(terms);
  }

  updateTypeSliderIsTV(val: boolean) {
    this.typeSliderIsTV.next(val);
  }

  updateTypeSliderIsMovie(val: boolean) {
    this.typeSliderIsMovie.next(val);
  }

  updateSelectedYears(years: number[] | undefined) {
    this.selectedYears.next(years);
  }

}