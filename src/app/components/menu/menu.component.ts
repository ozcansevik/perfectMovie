import { Component, OnInit } from '@angular/core';
import { MenuItem } from '../../../model/component/menuItem';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  menuItems : MenuItem[] = [];

  constructor() { }

  ngOnInit(): void {
    this.initializeMenuItems();
  }

  initializeMenuItems() {
    this.menuItems.push(new MenuItem('Movies'))
    this.menuItems.push(new MenuItem('Tv Shows'))
    this.menuItems.push(new MenuItem('My List', 'my-list'))
  }

  refreshPage() {
    window.location.replace('/')
  }

}
