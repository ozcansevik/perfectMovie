import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ItemDetailComponent } from './pages/item-detail/item-detail.component';
import { MyListComponent } from './pages/my-list/my-list.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'detail/:type/:id', component: ItemDetailComponent},
  {path: 'my-list', component: MyListComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
