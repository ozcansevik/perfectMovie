export class MenuItem {
    id: string = '';
    text: string = '';
    link: string | undefined = '';

    constructor (text: string, link?: string , id?: string){
        this.id = id ? id: '';
        this.text = text;
        this.link = link? link : undefined;
    }
}