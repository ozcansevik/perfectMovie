import { Component, OnInit, Input } from '@angular/core';
import { MovieService } from 'src/app/services/movie.service';
import { Observable, forkJoin } from 'rxjs';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'gallery-list',
  templateUrl: './gallery-list.component.html',
  styleUrls: ['./gallery-list.component.scss']
})
export class GalleryListComponent implements OnInit {

  /* Attributes */
  movieList: any;
  filteredMovieList: any;
  loading = false;
  galeryItemLimit = 10;


  /* Getters / Setters */
  private _searchTerms: string | undefined;
 
  @Input() set searchTerms(searchTerms: string | undefined) {
    this._searchTerms = searchTerms;
    if (searchTerms !== undefined && searchTerms.length > 0) {
      setTimeout(() => {
        this.searchService.updateSearchTerms(searchTerms);
        this.searchMedia();
      }, 250);
    } else if (searchTerms !== undefined && searchTerms.length === 0) {
      this.searchService.updateSearchTerms(searchTerms);
      if ((this.typeSliderIsMovie && this.typeSliderIsTV) ||
      (!this.typeSliderIsMovie && !this.typeSliderIsTV)) {
        this.discoverContents()
      } else  if (this.typeSliderIsTV) {
        this.discoverTVShows();
      } else {
        this.discoverMovies();
      }
    }
  }
  
  get searchTerms(): string | undefined {
    return this._searchTerms;
  }

  private _typeSliderIsTV: boolean | undefined;

  @Input() set typeSliderIsTV(typeSliderIsTV: boolean | undefined) {
    if (typeSliderIsTV !== undefined) {
      this._typeSliderIsTV = typeSliderIsTV;
      this.searchService.updateTypeSliderIsTV(typeSliderIsTV);
      this.applySearchAndFilters();
    }
  }

  get typeSliderIsTV(): boolean | undefined {
    return this._typeSliderIsTV;
  }

  
  private _typeSliderIsMovie: boolean | undefined;

  @Input() set typeSliderIsMovie(typeSliderIsMovie: boolean | undefined) {
    if (typeSliderIsMovie !== undefined) {
      this._typeSliderIsMovie = typeSliderIsMovie;
      this.searchService.updateTypeSliderIsMovie(typeSliderIsMovie);
      this.applySearchAndFilters();
    }
  }

  get typeSliderIsMovie(): boolean | undefined {
    return this._typeSliderIsMovie;
  }


  private _selectedYears: number[] | undefined = [];

  @Input() set selectedYears(selectedYears: number[] | undefined) {
    this._selectedYears = selectedYears;

    if (selectedYears !== undefined) {
      this.searchService.updateSelectedYears(selectedYears);

      this.filteredMovieList = this.movieList.filter((m: any) => {
        const movieYear = this.extractYear(m);
        const isInSelectedYears = selectedYears.find((year: number) => year === movieYear) !== undefined;
        return isInSelectedYears;
      });
    }
  }

  get selectedYears(): number[] | undefined {
    return this._selectedYears;
  }


  constructor(private movieService: MovieService, private searchService: SearchService) {
    this.movieList = []
  }

  ngOnInit(): void {
    this.searchTerms = this.searchService.searchTerms.getValue();
    this.selectedYears = this.searchService.selectedYears.getValue();

    if (this.testAllFilters()) {
      this.discoverContents()
    }
  }

  testAllFilters() {
    return this.searchTerms == undefined
          && this.typeSliderIsTV == undefined
          && this.typeSliderIsMovie == undefined
  }

  getAppropriateList() {
    return this.filteredMovieList && this.filteredMovieList.length > 0? this.filteredMovieList : this.movieList;
  }

  applySearchAndFilters() {
    if (this.searchTerms !== undefined && this.searchTerms.length > 0) {
      if ((this.typeSliderIsMovie && this.typeSliderIsTV) ||
      (!this.typeSliderIsMovie && !this.typeSliderIsTV)) {
        this.searchMedia();
      } else if (this.typeSliderIsTV) {
        this.searchMedia('tv');
      } else {
        this.searchMedia('movie');
      }
    } else {
      if ((this.typeSliderIsMovie && this.typeSliderIsTV) ||
      (!this.typeSliderIsMovie && !this.typeSliderIsTV)) {
        this.discoverContents()
      } else  if (this.typeSliderIsTV) {
        this.discoverTVShows();
      } else {
        this.discoverMovies();
      }
    }
  }

  extractYear(media: any) {
    const getYear = (date: string) => { return (date ? new Date(date).getFullYear() : null) }
    return media.first_air_date? getYear(media.first_air_date) : getYear(media.release_date)
  }

  discoverContents() {
    this.loading = true;

    const years: number[] = []
    this.movieService.discoverMovies().subscribe((res: any) => {
      const obs: Observable<any>[] = []
      res.results.forEach((movie: any) => {
        if (obs.length <= ((this.galeryItemLimit / 2) - 1) ) {
          const year = this.extractYear(movie);
          if (year) {
            years.push(year);
          }
          obs.push(this.movieService.getMovieDetail(movie.id));
        }
      })

      this.movieService.discoverTVShow().subscribe((resTV: any) => {
        resTV.results.forEach((tvShow: any) => {
          if (obs.length <= this.galeryItemLimit - 1) {
            const year = this.extractYear(tvShow);
            if (year) {
              years.push(year);
            }
            obs.push(this.movieService.getTvDetail(tvShow.id));
          }
        })

        forkJoin(obs).subscribe((res: any) => {
          this.movieList = res;
          this.movieService.updateAvailaibleYears(years);
          this.loading = false;
        })
      });
    });
  }

  discoverMovies() {
    this.loading = true;
    const years: number[] = [];
    const obs: Observable<any>[] = []
    this.movieService.discoverMovies().subscribe((res: any) => {
      res.results.forEach((movie: any) => {
        if (obs.length <= this.galeryItemLimit - 1) {
          const year = this.extractYear(movie);
          if (year) {
            years.push(year);
          }
          obs.push(this.movieService.getMovieDetail(movie.id));
        }
      })

      forkJoin(obs).subscribe((res: any) => {
        this.movieList = res;
        this.movieService.updateAvailaibleYears(years);
        this.loading = false;
      })
    });
  }

  discoverTVShows() {
    this.loading = true;
    const years: number[] = [];
    const obs: Observable<any>[] = []
    this.movieService.discoverTVShow().subscribe((resTV: any) => {
      resTV.results.forEach((tvShow: any) => {
        if (obs.length <= this.galeryItemLimit - 1) {
          const year = this.extractYear(tvShow);
          if (year) {
            years.push(year);
          }
          obs.push(this.movieService.getTvDetail(tvShow.id));
        }
      })

      forkJoin(obs).subscribe((res: any) => {
        this.movieList = res;
        this.movieService.updateAvailaibleYears(years);
        this.loading = false;
      })
    });
  }

  searchMedia(mediaTypeWanted?: string) {
    this.loading = true;
    const years: number[] = [];

    if (this.searchTerms !== undefined) {
      this.movieService.getMovies(this.searchTerms).subscribe((res: any) => {
        const obs: Observable<any>[] = []
        res.results.forEach((movie: any) => {
          if (movie.media_type) {
            switch (movie.media_type) {
              case 'tv':
                  if (!mediaTypeWanted || mediaTypeWanted === 'tv') {
                    if (obs.length <= this.galeryItemLimit - 1) {
                      const year = this.extractYear(movie);
                      if (year) {
                        years.push(year);
                      }
                      obs.push(this.movieService.getTvDetail(movie.id));
                    }
                  }
              break;
              case 'movie':
                  if (!mediaTypeWanted || mediaTypeWanted === 'movie') {
                    if (obs.length <= this.galeryItemLimit - 1) {
                      const year = this.extractYear(movie);
                      if (year) {
                        years.push(year);
                      }
                      obs.push(this.movieService.getMovieDetail(movie.id));
                    }
                  }
              break;
            }
          }
        })
  
        forkJoin(obs).subscribe((res: any) => {
          this.movieList = res;
          this.movieService.updateAvailaibleYears(years);
          this.loading = false;
        }, () => {
          this.loading = false;
        })
        }, () => {
          this.loading = false;
        }, () => {
          this.loading = false;
        });
    }
  }

}
