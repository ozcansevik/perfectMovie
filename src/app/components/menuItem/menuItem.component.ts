import { Component, OnInit, Input } from '@angular/core';
import { MenuItem } from '../../../model/component/menuItem';
import { Router } from '@angular/router';

@Component({
  selector: 'menu-item',
  templateUrl: './menuItem.component.html',
  styleUrls: ['./menuItem.component.scss']
})
export class MenuItemComponent implements OnInit {

  @Input() item: MenuItem = new MenuItem('');

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToLink() {
    if (this.item.link) {
      this.router.navigate(['/my-list/']);
    }
  }

}
