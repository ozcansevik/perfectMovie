import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { WatchListService } from 'src/app/services/watchList.service';
import { Router } from '@angular/router';

@Component({
  selector: 'poster',
  templateUrl: './poster.component.html',
  styleUrls: ['./poster.component.scss']
})
export class PosterComponent implements OnInit {

  @Input() allowHover : boolean | undefined;
  @Input() media : any;
  @Input() customClass : string | undefined;
  @HostBinding('class') classList = 'poster-component';

  isWatched = false;
  isToWatch = false;

  alreadyWatchedList: [] | undefined
  toWatchList: [] | undefined

  constructor(
    private watchListService: WatchListService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (this.customClass) {
      this.classList += ' ' + this.customClass;
    }

    // Subscribe to watchList data
    this.watchListService.alreadyWatchedListObs.subscribe((res: any) => {
      this.alreadyWatchedList = res;
    });

    this.watchListService.toWatchListObs.subscribe((res: any) => {
      this.toWatchList = res;
    });

  }

  alreadyWatch() {
    this.watchListService.alreadyWatch(this.media);
  }

  toWatch() {
    this.watchListService.toWatch(this.media);
  }

  isInAlreadyWatchedList() {
    if (this.alreadyWatchedList) {
      return this.alreadyWatchedList
      .find((media: any) => media.id == this.media.id) !== undefined;
    }
    return false;
  }

  isInToWatchList() {
    if (this.toWatchList) {
      return this.toWatchList
      .find((media: any) => media.id == this.media.id) !== undefined;
    }
    return false;
  }

  goToMediaDetail() {
    const type = this.media.title ? 'movie' : 'tv';
    this.router.navigate(['/detail/' + type + '/' + this.media.id]);
  }

}
