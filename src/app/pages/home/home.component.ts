import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  searchTerms: string | undefined;
  typeSliderIsTV: boolean | undefined;
  typeSliderIsMovie: boolean | undefined;
  selectedYears: number[]  | undefined;

  constructor() { }

  ngOnInit(): void {
  }

  updateSearchTerms(ev: string) {
    this.searchTerms = ev;
  }

  updateSliderIsTV(ev: boolean) {
    this.typeSliderIsTV = ev;
  }

  updateSliderIsMovie(ev: boolean) {
    this.typeSliderIsMovie = ev;
  }

  updateSelectedYears(ev: number[]) {
    this.selectedYears = ev;
  }

}
