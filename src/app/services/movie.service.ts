import { HttpClient } from '@angular/common/http';
import { APIKEY } from '../conf/conf';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Movie } from 'src/model/logical/movie';
import { TvShow } from 'src/model/logical/tvShow';

@Injectable({
    providedIn: 'root',
})
export class MovieService {

  private apiURL: string = 'https://api.themoviedb.org/3/';

  private availaibleYears = new BehaviorSubject<number[]>([]);
  availaibleYearsObs = this.availaibleYears.asObservable();

  constructor(private httpClient: HttpClient) {}

  buildUrl(apiPath: string, apiExtra?: string) {
      let url = this.apiURL + apiPath + '?api_key=' + APIKEY;
      if (apiExtra) {
          url += apiExtra;
      }
      return url;
  }

  /* DISCOVER API */
  discoverMovies() {
      return this.httpClient.get(this.buildUrl('discover/movie/'));
  }

  discoverTVShow() {
      return this.httpClient.get(this.buildUrl('discover/tv/'));
  }

  /* MOVIES API */
  getMovies(searchWords: string) {
      return this.httpClient.get(this.buildUrl('search/multi/', '&query=' + searchWords));
  }

  getMovieDetail(id: string) : Observable<Movie> {
      return this.httpClient.get(this.buildUrl('movie/' + id)) as Observable<Movie>;
  }

  getMovieVideos(id: string) {
    return this.httpClient.get(this.buildUrl('movie/' + id + '/videos'));
  }

  getMovieCredits(id: string) {
      return this.httpClient.get(this.buildUrl('movie/' + id + '/credits'));
  }

  /* TV DETAIL API */
  getTvDetail(id: string): Observable<TvShow> {
      return this.httpClient.get(this.buildUrl('tv/' + id)) as Observable<TvShow>;
  }

  getTvVideos(id: string) {
    return this.httpClient.get(this.buildUrl('tv/' + id + '/videos'));
  }

  getTvCredits(id: string) {
      return this.httpClient.get(this.buildUrl('tv/' + id + '/credits'));
  }

  /* PROVIDE AVAILABLE YEARS FROM MOVIE OR TV SHOW LIST, TO ENTIRE APP */
  updateAvailaibleYears(years: number[]) {
      let uniqArrayYears = [...new Set(years)]
      uniqArrayYears = uniqArrayYears.sort().reverse();
      this.availaibleYears.next(uniqArrayYears)
  }

}