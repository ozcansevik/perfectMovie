import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import * as TimeLib from 'src/lib/time'

@Component({
  selector: 'gallery-item',
  templateUrl: './gallery-item.component.html',
  styleUrls: ['./gallery-item.component.scss']
})
export class GalleryItemComponent implements OnInit {

  @Input() allowHover : boolean | undefined;
  @Input() media: any;

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToMediaDetail() {
    const type = this.media.title ? 'movie' : 'tv';
    this.router.navigate(['/detail/' + type + '/' + this.media.id]);
  }

  timeConvert(num: number) {
    return TimeLib.time_convert(num);
  }

}
