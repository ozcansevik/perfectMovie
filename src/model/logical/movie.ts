export class Movie {
    adult: boolean = false;
    backdrop_path: string | null | undefined;
    belongs_to_collection: any;
    budget: number | undefined;
    genres: Array<any> | undefined;
    homepage: string | null | undefined;
    id: string | undefined;
    imdb_id: string | null | undefined;
    original_language: string | undefined;
    original_title: string | undefined;
    overview: string | null | undefined;
    popularity: number | undefined;
    poster_path: string | null | undefined;
    production_companies: Array<any> | undefined;
    production_countries: Array<any> | undefined;
    release_date: string | undefined;
    revenue: number | undefined;
    runtime: number | null | undefined;
    spoken_languages: Array<any> | undefined;
    status: string | undefined;
    tagline: string | null | undefined;
    title: string | undefined;
    video: boolean | undefined;
    vote_average: number | undefined;
    vote_count: number | undefined;
}