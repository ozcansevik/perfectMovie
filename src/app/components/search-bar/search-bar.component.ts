import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MovieService } from 'src/app/services/movie.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SearchBarComponent implements OnInit {

  myControl = new FormControl();
  options: [] = [];
  availaibleYears: number[] = [];
  searchTerms: string | undefined = '';
  typeSliderIsMovie: boolean| undefined = false
  typeSliderIsTVShow:  boolean | undefined = false

  selectedYears: number[]  | undefined = [];

  @Output() searchTermsChange: EventEmitter<any> = new EventEmitter();

  @Output() sliderIsTV: EventEmitter<any> = new EventEmitter();
  @Output() sliderIsMovie: EventEmitter<any> = new EventEmitter();
  @Output() updateSelectedYears: EventEmitter<any> = new EventEmitter();

  
  constructor(private movieService: MovieService, private searchService: SearchService) {
  }

  ngOnInit(): void {
    this.searchTerms = this.searchService.searchTerms.getValue();

    this.movieService.availaibleYearsObs.subscribe(years => {
      this.availaibleYears = years;

        setTimeout(() => {
          this.selectedYears = [];
          this.updateSelectedYears.emit(this.selectedYears);
        }, 100);
    })
  }

  searchInputChanged() {
    this.searchTermsChange.emit(this.searchTerms)
  }

  typeSliderTVChanged() {
    this.sliderIsTV.emit(this.typeSliderIsTVShow)
  }

  typeSliderMovieChanged() {
    this.sliderIsMovie.emit(this.typeSliderIsMovie)
  }

  selectYear(year: number) {
    if (this.selectedYears !== undefined) {
      const idx = this.selectedYears.findIndex((y: number) => y === year);
      if (idx != -1) {
        this.selectedYears.splice(idx, 1);
      } else {
        this.selectedYears.push(year);
      }
      this.selectedYears = [...new Set(this.selectedYears)];
      this.updateSelectedYears.emit(this.selectedYears);
    }
  }

  isSelectedYear(year: number) {
    return this.selectedYears ? 
            this.selectedYears.find((y: number) => y === year) !== undefined
            : false;
  }

}
