import { HttpClient } from '@angular/common/http';
import { APIKEY } from '../conf/conf';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class WatchListService {

  ALREADY_WATCH = 'alreadyWatch';
  TO_WATCH = 'toWatch';

  private alreadyWatchedList = new BehaviorSubject<[]>([]);
  alreadyWatchedListObs = this.alreadyWatchedList.asObservable();

  private toWatchList = new BehaviorSubject<[]>([]);
  toWatchListObs = this.toWatchList.asObservable();

  constructor() {
      const alreadyWatched = this.readInSessionStorage(this.ALREADY_WATCH);
      const toWatch = this.readInSessionStorage(this.TO_WATCH);
      this.alreadyWatchedList.next(alreadyWatched);
      this.toWatchList.next(toWatch);
  }

  alreadyWatch(media: any) {
      let alreadyWatch : any = this.readInSessionStorage(this.ALREADY_WATCH);
      if (alreadyWatch) {
          alreadyWatch = this.addOrRemoveItemList(alreadyWatch, media);
      }  
      this.writeInSessionStorage(this.ALREADY_WATCH, alreadyWatch);
  }
  
  toWatch(media: any) {
      let toWatch : any = this.readInSessionStorage(this.TO_WATCH);
      if (toWatch) {
          toWatch = this.addOrRemoveItemList(toWatch, media);
      }  
      this.writeInSessionStorage(this.TO_WATCH, toWatch);
  }

  addOrRemoveItemList(list: any, media: any) {
      const idx = list.findIndex((m: any) => m.id.toString() === media.id.toString());
      if (idx != -1) {
          list.splice(idx, 1);
      } else {
          list.push(media);
      }
      return list;
  }

  readInSessionStorage(key: string) {
      const readedData = localStorage.getItem(key)? 
      localStorage.getItem(key) as string : null;
      if (readedData) {
          return JSON.parse(readedData);
      } else {
          return []
      }
  }

  writeInSessionStorage(key: string, data: any) {
      if (key === this.ALREADY_WATCH) {
          this.alreadyWatchedList.next(data);
      } else if (key === this.TO_WATCH) {
          this.toWatchList.next(data);
      }

      localStorage.setItem(key, JSON.stringify(data));
  }

}