import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieService } from 'src/app/services/movie.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Movie } from 'src/model/logical/movie';
import { TvShow } from 'src/model/logical/tvShow';
import * as TimeLib from 'src/lib/time'

@Component({
  selector: 'item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.scss']
})
export class ItemDetailComponent implements OnInit {

  movie: any;
  movieId: string = '';
  movieCredits: any;
  movieVideo: any;
  isMovie = false;

  BASE_URL_VIDEO = 'https://www.youtube.com/embed/'

  constructor(
    private movieService: MovieService, 
    private route: ActivatedRoute,
    private _sanitizer: DomSanitizer
  ) { }

  ngOnInit(): void {
    document.body.scrollTop = document.documentElement.scrollTop = 0;

    this.movieId = this.route.snapshot.params['id'];
    this.isMovie = this.route.snapshot.params['type'] ? this.route.snapshot.params['type'] === 'movie' : false;
    
    if (this.isMovie) {
      this.movieService.getMovieDetail(this.movieId).subscribe((movie: Movie) => {
        this.movie = movie;

        this.movieService.getMovieVideos(this.movieId).subscribe((res: any) => {
          this.movieVideo = this._sanitizer.bypassSecurityTrustResourceUrl(this.BASE_URL_VIDEO + res.results[0].key);
        });
  
        this.movieService.getMovieCredits(this.movieId).subscribe((movieCredits: any) => {
          this.movieCredits = movieCredits;
        });
      });
    } else {  // tv show
      this.movieService.getTvDetail(this.movieId).subscribe((tv: TvShow) => {
        this.movie = tv;

        this.movieService.getTvVideos(this.movieId).subscribe((res: any) => {
          this.movieVideo = this._sanitizer.bypassSecurityTrustResourceUrl(this.BASE_URL_VIDEO + res.results[0].key);
        });
  
        this.movieService.getTvCredits(this.movieId).subscribe((tvCredits: any) => {
          this.movieCredits = tvCredits;
        });
      });
    }
    
  }

  getDirectors() {
    if (this.movieCredits) {
      if (this.isMovie) {
        return this.movieCredits.crew.filter((crew: any) => crew.job === 'Director');
      } else { // tv show
        return this.movieCredits.crew.filter((crew: any) => crew.job === 'Executive Producer');
      }
    }
  }

  getCast() {
    if (this.movieCredits) {
      let cast = this.movieCredits.cast.filter((crew: any) => crew.known_for_department === 'Acting')
      cast.sort((c: any) => c.popularity);
      if (cast.length > 10) {
        cast = cast.slice(0, 10);
      }
      return cast;
    }
  }

  timeConvert(num: number) {
    return TimeLib.time_convert(num);
  }


}
