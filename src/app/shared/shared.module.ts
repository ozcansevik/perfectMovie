import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import {MatFormFieldModule} from '@angular/material/form-field'
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatChipsModule} from '@angular/material/chips';

/* CUSTOM COMPONENT */
import { HomeComponent } from '../pages/home/home.component';
import { MyListComponent } from '../pages/my-list/my-list.component';
import { ItemDetailComponent } from '../pages/item-detail/item-detail.component';
import { MenuComponent } from '../components/menu/menu.component';
import { MenuItemComponent } from '../components/menuItem/menuItem.component';
import { SearchBarComponent } from '../components/search-bar/search-bar.component';
import { GalleryListComponent } from '../components/gallery-list/gallery-list.component';
import { LoaderComponent } from '../components/loader/loader.component';
import { PosterComponent } from '../components/poster/poster.component';
import { GalleryItemComponent } from '../components/gallery-item/gallery-item.component';


@NgModule({
  declarations: [
    HomeComponent,
    MenuComponent,
    MenuItemComponent,
    SearchBarComponent,
    GalleryListComponent,
    GalleryItemComponent,
    LoaderComponent,
    ItemDetailComponent,
    PosterComponent,
    MyListComponent
  ],
  imports: [
    CommonModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSlideToggleModule,
    MatChipsModule
  ],
  exports: [
    HomeComponent,
    MenuComponent,
    MenuItemComponent,
    SearchBarComponent,
    GalleryListComponent,
    GalleryItemComponent,
    LoaderComponent,
    ItemDetailComponent,
    PosterComponent,
    MyListComponent
  ]
})
export class SharedModule { }
