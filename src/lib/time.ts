export function time_convert(num: number)
 { 
  const hours = Math.floor(num / 60);  
  const minutes = num % 60;

  const date = new Date();
  date.setHours(hours);
  date.setMinutes(minutes);

  return date;       
}