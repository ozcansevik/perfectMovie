import { Component, OnInit } from '@angular/core';
import { WatchListService } from 'src/app/services/watchList.service';

@Component({
  selector: 'my-list',
  templateUrl: './my-list.component.html',
  styleUrls: ['./my-list.component.scss']
})
export class MyListComponent implements OnInit {

  alreadyWatchedList: any;
  toWatchList: any;

  constructor(private watchListService: WatchListService) { }

  ngOnInit(): void {
      // Subscribe to watchList data
      this.watchListService.alreadyWatchedListObs.subscribe((res: any) => {
        this.alreadyWatchedList = res;
      });

      this.watchListService.toWatchListObs.subscribe((res: any) => {
        this.toWatchList = res;
      });
  }

}
